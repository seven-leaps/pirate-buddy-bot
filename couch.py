import commonfunctions
import requests
import urllib
import telegram
import json

couch_potato_api_url = ""

def setUrl(url):
    global couch_potato_api_url
    couch_potato_api_url = url

def handleCommand(bot, update, args):
    if commonfunctions.isUserAuthenticated(update):
        couch_event = args[0]
        if(couch_event == "search"):
            query = getSearchString(args)
            couchPotatoSearch(bot, update, query)
        elif(couch_event == "add"):
            couchPotatoAdd(bot, update, args)
        else:
            message = "Unknown command: %s" % couch_event
            commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

    else:
        commonfunctions.handleUnknownUser(bot, update)

def couchPotatoSearch(bot, update, query):
    encoded_query = urllib.quote(query.encode('utf8'))
    search_url = couch_potato_api_url + "search?q=" + encoded_query

    message = 'Searching for "%s" ...' % query
    commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

    bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    search_response = requests.get(search_url)

    if(search_response.ok):
        search_response_json = json.loads(search_response.content)
        movies = search_response_json['movies']

        custom_keyboard = []
        for movie in movies:
            if ('imdb' in movie) and ('titles' in movie):
                imdb = movie['imdb']
                title = movie['titles'][0]
                year = str(movie['year'])
                custom_keyboard.append(["/couch add \"" + title + "\" (" + year + ") " + imdb])

        if(len(custom_keyboard) > 0):
            message = '"%s" returned the following results, click option to add to watch list' % query
            commonfunctions.sendMessageWithCustomKeyboard(bot, update, message, custom_keyboard)
        else:
            message = 'Found no results for "%s"' % query
            commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

    else:
        commonfunctions.sendMessageWithHideKeyboard(bot, update, 'Search Failed.')

def couchPotatoAdd(bot, update, args):

    print "args: %s" % args[1:(len(args))-2]
    spacing = " ";
    #2nds last is year, last is IMDB number
    movie_title = spacing.join( args[1:(len(args))-2] )

    #hmm, converting string to array removing first and last elements (which are ") and converting back to string
    movie_title = "".join(list(movie_title)[1:(len(movie_title))-1])

    message = 'Adding "%s" ...' % movie_title
    commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

    bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    encoded_movie_title = urllib.quote(movie_title.encode('utf8'))
    imdb = args[len(args) - 1]

    add_url = couch_potato_api_url + "movie.add?profile_id=&category_id=&force_readd=&title=" + encoded_movie_title + "&identifier=" + imdb

    print 'sending add request to CP: %s' % add_url

    add_response = requests.get(add_url)

    if(add_response.ok):
        add_response_json = json.loads(add_response.content)
        if 'movie' in add_response_json:
            response_message = "Added \"%s\" to CouchPotato!" % add_response_json['movie']['title']
        else:
            response_message = "Failed to add to CouchPotato"

    else:
        response_message = "Failed to add to couchPotato"

    commonfunctions.sendMessageWithHideKeyboard(bot, update, response_message)

def getSearchString(args):
    spacing = " ";
    query = spacing.join( args[1:(len(args))] )
    return query
