import commonfunctions
import telegram
import re
from time import time
import wget

watch_folders = {}

def setWatchFolders(folders):
    global watch_folders
    watch_folders = folders

def handleCommand(bot, update, args):
    user = update.message.from_user.id
    print "Recieved torrent command from user id %s" % user
    if commonfunctions.isUserAuthenticated(update):
        processTorrent(bot, update, args)
    else:
        commonfunctions.handleUnknownUser(bot, update)

def listWatchOptions(bot, update):
    if commonfunctions.isUserAuthenticated(update):
        if(bool(watch_folders)):
            message = "The following watch folders are configured:\n"
            for folder in watch_folders:
                message = message + "\n - " + folder
            bot.sendMessage(chat_id=update.message.chat_id, text=message, parse_mode=telegram.ParseMode.MARKDOWN)
        else:
            commonfunctions.sendMessageWithHideKeyboard(bot, update, 'No watch folders set in config.json.')

    else:
        commonfunctions.handleUnknownUser(bot, update)

def processMagnetLinkFromNormalMessage(bot, update, magnet_link):
    name = getNameFromMagnet(magnet_link)
    custom_keyboard = []
    if(bool(watch_folders)):
        message = "Got magnet link for %s, where do you want to put it?" % name
        updateLastLinkRecieved(magnet_link)
        for folder in watch_folders:
            custom_keyboard.append(["/torrent " + folder + " cache"])

        commonfunctions.sendMessageWithCustomKeyboard(bot, update, message, custom_keyboard)
    else:
        commonfunctions.sendMessageWithHideKeyboard(bot, update, 'No watch folders set in config.json.')

def updateLastLinkRecieved(link):
    global last_link_recieved
    last_link_recieved = link

def checkForMagnetLink(text):
    return re.match( r'^magnet:\?', text, re.M|re.I)

def processTorrent(bot, update, args):
    target = args[0]
    torrent = args[1]

    if(torrent == "cache"):
        torrent = last_link_recieved

    if target in watch_folders:
        if checkForMagnetLink(torrent):
            handleMagnet(bot, update, torrent, watch_folders[target])
        else:
            downloadTorrent(bot, update, torrent, watch_folders[target])
    else:
        message = 'Did not find "%s" in config.json'
        commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

def getNameFromMagnet(magnet_link):
    matchObj = re.search( r'dn=([^&]*)', magnet_link)
    if matchObj:
        return matchObj.group(1)
    else:
        return ""


def handleMagnet(bot, update, magnet_link, target_dest):
    time_stamp = str(time()).replace('.', '')
    target_file_name = "%s.magnet" % time_stamp

    download_name = getNameFromMagnet(magnet_link)
    if download_name:
        target_file_name = '%s.magnet' % download_name

    target_file = target_dest + target_file_name
    print "target_file: %s" % target_file

    with open(target_file,"w") as f:
        f.write(magnet_link)
        response_message = "Created magnet file %s and moved to %s" % (target_file_name, target_dest)
        commonfunctions.sendMessageWithHideKeyboard(bot, update, response_message)
    f.closed

def downloadTorrent(bot, update, torrent_url, target_dest):
    filename = wget.download(torrent_url)
    os.rename(filename, target_dest+filename)
    response_message = "Downloaded %s and moved to %s" % (filename, target_dest)
    commonfunctions.sendMessageWithHideKeyboard(bot, update, response_message)
