import commonfunctions
import requests
import urllib
import telegram
import json

plex_url = ""

def setUrl(url):
    global plex_url
    plex_url = url

def handleCommand(bot, update, args):
    if commonfunctions.isUserAuthenticated(update):
        plex_event = args[0]
        if(plex_event == "search"):
            query = getSearchString(args)
            plexSearch(bot, update, query)
        # elif(plex_event == "sections"):
        #     displayPlexSections(bot, update, args)
        else:
            message = "Unknown command: %s" % plex_event
            commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

    else:
        commonfunctions.handleUnknownUser(bot, update)

# def displayPlexSections(bot, update, args):
#     sections_url =

def plexSearch (bot, update, query):
    encoded_query = urllib.quote(query.encode('utf8'))
    search_url = plex_url + "search?query=" + encoded_query

    message = 'Searching for "%s" ...' % query
    commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

    bot.sendChatAction(chat_id=update.message.chat_id, action=telegram.ChatAction.TYPING)

    search_response = requests.get(search_url, headers={"Accept":"application/json"})

    if(search_response.ok):
        search_response_json = json.loads(search_response.content)
        results = search_response_json['_children']

        result_text = ""
        for result in results:
            if ('title' in result) and ('year' in result) and ('librarySectionTitle' in result):
                title = result['title']
                year = str(result['year'])
                library = result['librarySectionTitle']

                result_text = result_text + "- \"" + title + "\" [" + year + "] (" + library + ")\n"

        if(result_text):
            message = '"%s" returned the following results: \n\n' % query
            message = message + result_text
            commonfunctions.sendMessageWithHideKeyboard(bot, update, message)
        else:
            message = 'Found no results for "%s"' % query
            commonfunctions.sendMessageWithHideKeyboard(bot, update, message)

    else:
        commonfunctions.sendMessageWithHideKeyboard(bot, update, 'Search Failed.')

def getSearchString(args):
    spacing = " ";
    query = spacing.join( args[1:(len(args))] )
    return query
