#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.

"""
This Bot uses the Updater class to handle the bot.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import json
import os
from telegram import Updater
import telegram
import logging
import couch
import plex
import torrent
import commonfunctions


# Enable logging
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO)

logger = logging.getLogger(__name__)

users = []
download_folders = {}
last_link_recieved = ""


# Define a few command handlers. These usually take the two arguments bot and
# update. Error handlers also receive the raised TelegramError object in error.
def start(bot, update):
    bot.sendMessage(update.message.chat_id, text='Hi!')


def help(bot, update):

    message = "Welcome to pirate-buddy-bot! \n\n"
    message = message + "Torrents:\n"
    message = message + "Paste in magnet links to add to watch folder \n"
    message = message + "To list torrent watch folders - /watch\n"
    message = message + "To add torrents directly to watch folder - /torrent watch_folder torrent_link \n"

    message = message + "\n"
    message = message + "Couch Potato:\n"
    message = message + "To search for movies - /couch search movie title\n"
    message = message + "To add movies directly - /couch add movie title year imdb_number \n"
    reply_markup = telegram.ReplyKeyboardHide()
    bot.sendMessage(update.message.chat_id, text=message, reply_markup=reply_markup)


def normalMessage(bot, update):
    if commonfunctions.isUserAuthenticated(update):
        message = update.message.text
        if torrent.checkForMagnetLink(message):
            torrent.processMagnetLinkFromNormalMessage(bot, update, message)
    else:
        handleUnknownUser(bot, update)


def error(bot, update, error):
    logger.warn('Update "%s" caused error "%s"' % (update, error))


def test(bot, update):
    print "Update: %s" % update
    print "Bot: %s" % bot


def main():

    if os.path.isfile('devConfig.json'):
        config_file_name = 'devConfig.json'
    else:
        config_file_name = 'config.json'


    with open(config_file_name) as config_file:
            config = json.load(config_file)
            print "Reading config from %s." % config_file_name

    bot_token = config['BOT_TOKEN'];
    bot_token = os.environ.get('BOT_TOKEN', bot_token)
    print "Bot Key %s." % bot_token

    users = config['users']
    commonfunctions.setAuthenticatedUsers(users)

    watch_folders = config['torrent']['watchFolders']
    torrent.setWatchFolders(watch_folders)

    cp_key = config['couchPotato']['apiKey']
    cp_url = config['couchPotato']['serverAddress']

    couch_potato_api_url = cp_url + "api/" + cp_key + "/"
    couch.setUrl(couch_potato_api_url)

    plex_url = config['plex']['serverAddress']

    plex.setUrl(plex_url)

    #print "dl %s" % download_folders

    if(bot_token == 'INSERT_BOT_KEY_HERE' or bot_token == ''):
        print "Please add a BOT-KEY to config.json and restart."
    else:
        # Create the EventHandler and pass it your bot's token.
        updater = Updater(bot_token)

        # Get the dispatcher to register handlers
        dp = updater.dispatcher

        # on different commands - answer in Telegram
        dp.addTelegramCommandHandler("start", help)
        dp.addTelegramCommandHandler("help", help)
        dp.addTelegramCommandHandler("torrent", torrent.handleCommand)
        dp.addTelegramCommandHandler("watch", torrent.listWatchOptions)
        dp.addTelegramCommandHandler("test", test)

        dp.addTelegramCommandHandler("couch", couch.handleCommand)
        dp.addTelegramCommandHandler("plex", plex.handleCommand)

        # on noncommand i.e message - echo the message on Telegram
        dp.addTelegramMessageHandler(normalMessage)

        # log all errors
        dp.addErrorHandler(error)

        # Start the Bot
        updater.start_polling()

        # Run the bot until the you presses Ctrl-C or the process receives SIGINT,
        # SIGTERM or SIGABRT. This should be used most of the time, since
        # start_polling() is non-blocking and will stop the bot gracefully.
        updater.idle()

if __name__ == '__main__':
    main()
