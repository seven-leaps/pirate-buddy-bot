import telegram

authenticated_users = []

def setAuthenticatedUsers(users):
    global authenticated_users
    authenticated_users = users

def isUserAuthenticated(update):
    user = update.message.from_user.id
    return user in authenticated_users

def handleUnknownUser(bot, update):
    message = "Not a known user, Add telegram id (%s) to config.json" % update.message.from_user.id
    print message
    bot.sendMessage(update.message.chat_id, text=message)

def sendMessageWithHideKeyboard(bot, update, message):
    reply_markup = telegram.ReplyKeyboardHide()
    bot.sendMessage(update.message.chat_id, text=message, reply_markup=reply_markup)

def sendMessageWithCustomKeyboard(bot, update, message, custom_keyboard):
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard)
    bot.sendMessage(update.message.chat_id, text=message, reply_markup=reply_markup)
