# Dependancy #

* Install Pyhton
* Install [PIP](http://python-packaging-user-guide.readthedocs.org/en/latest/installing/#install-pip-setuptools-and-wheel)
* Install pip dependancies: sudo pip install -r requirements.txt

BOT_TOKEN is taken from environment variable first, else its read from config.json


To run in the background:

- nohup python pirate-buddy-bot.py &

If you are getting an error message about "Conflict: terminated by other..":

- sudo ps -A l | grep pirate

kill all the pids that show up there:

- kill pid
